import numpy as np

def generate_sinusoid(n=100):
    """
    Generates 
    """
    t = np.random.uniform(0, 2.*np.pi, size=(n,))
    
    x = np.sin(t)
    y = np.cos(t)
    z = 1/10*np.sin(150*t)
    
    return np.array([x, y, z])

def generate_spheres(d = 2, r = 1, n = 200):
    """
    Generates a d dimensional sphere in a d + 1 dimensional ambient space.
    
    :param d: dimension of n-sphere
    :param r: radius size
    :param n: cardinality
    :return: np.Array system of parametric equations for a n-sphere
    """
    indep_var = [None] * (d+1)
    for i in range(1, d+1):
        if (i % 2) == 1:
            indep_var[i] = np.random.uniform(0, 2.*np.pi, size=(n,))
        else:
            indep_var[i] = indep_var[i-1]
    indep_var[0] = np.random.uniform(-0.5*np.pi, 0.5*np.pi, size=(n,))
    
    init = np.sin(indep_var[0])
    matrix = np.array([init])
    recursive = [1]*n
    for idx, val in enumerate(indep_var[1:]):
        if idx == 0:
            recursive = np.cos(indep_var[0])
            tmp = recursive * np.sin(val)
        elif (idx % 2) == 1:
            tmp = recursive * np.cos(val)
            recursive = tmp
        else:
            tmp = recursive * np.sin(val)
        matrix = np.append(matrix, [tmp], axis=0)
    return matrix

def generate_k_mobius(n = 200, k = 1, noise = 0):
    u = np.random.uniform(-1, 1, size=(n,))
    v = np.random.uniform(0, 2.*np.pi, size=(n,))
    common = (1 + u / 2 * np.cos(k * v / 2 ))
    x = common * np.cos(v)
    y = common * np.sin(v)
    z = u / 2 * np.sin( k * v / 2)
    
    mobius = np.array([x, y, z])
    if noise != 0:
        for i in range(len(mobius)):
            mobius[i] += np.random.normal(0, noise, size = (n,))
    return mobius

def generate_d_indep_var(d, n, ran=(0, 1), func = np.vectorize(lambda x : x)):
    indep_var = [None] * (d)
    for i in range(d):
        indep_var[i] = func(np.random.uniform(ran[0], ran[1], size=(n,)))
    return indep_var

def generate_12_manifold(n = 1200):
    indep_var = generate_d_indep_var(12, n)
    param = [None]*72
    for i in range(11):
        param[2*i] = indep_var[i + 1] * np.cos(2. * np.pi * indep_var[i])
        param[2*i + 1] = indep_var[i + 1] * np.sin(2. * np.pi * indep_var[i])
    param[2*11] = indep_var[0] * np.cos(2. * np.pi * indep_var[11])
    param[2*11 + 1] = indep_var[0] * np.sin(2. * np.pi * indep_var[11]) 

    for j in range(24, 72):
        param[j] = param[j-24]
    return np.array(param)

def generate_affine_3_5_space(n = 200):
    indep_var = generate_d_indep_var(3, n)
    s = indep_var[0]
    t = indep_var[1]
    u = indep_var[2]
    
    x1 = 1.2*s + 0.5*t + 3
    x2 = 0.5*s + 0.9*u - 1
    x3 = -0.5*s - 0.2*t + u
    x4 = 0.4*s - 0.9*t - 0.1*u
    x5 = 1.1*s - 0.3*u + 8
    
    return np.array([x1, x2, x3, x4, x5])

def generate_4_6_confusable_figure(n = 200):
    indep_var = generate_d_indep_var(4, n)
    
    x1 = indep_var[1]**2 * np.cos(2.*np.pi*indep_var[0])
    x2 = indep_var[2]**2 * np.sin(2.*np.pi*indep_var[0])
    x3 = indep_var[1] + indep_var[2] + np.power((indep_var[1] - indep_var[3]), 2)
    x4 = indep_var[1] - 2*indep_var[2] + np.power((indep_var[0] - indep_var[3]), 2)
    x5 = -indep_var[1] - 2*indep_var[2] + np.power((indep_var[2] - indep_var[3]), 2)
    x6 = indep_var[0] ** 2 - indep_var[1]**2 + indep_var[2] ** 2 - indep_var[3] ** 2
    
    return np.array([x1, x2, x3, x4, x5, x6])

def generate_affine_4_8_space(n = 200):
    indep_var = generate_d_indep_var(4, n)
    s = indep_var[0]
    t = indep_var[1]
    u = indep_var[2]
    v = indep_var[3]
    
    x1 = t * np.cos(2. * np.pi * s)
    x2 = t * np.sin(2. * np.pi * s)
    x3 = u * np.cos(2. * np.pi * t)
    x4 = u * np.sin(2. * np.pi * t)
    x5 = v * np.cos(2. * np.pi * u)
    x6 = v * np.sin(2. * np.pi * u)
    x7 = s * np.cos(2. * np.pi * v)
    x8 = s * np.sin(2. * np.pi * v)
    
    return np.array([x1, x2, x3, x4, x5, x6, x7, x8])

def generate_helix_2_3(n = 200):
    r = 2. * np.pi * np.random.uniform(0, 1, size=(n,)) - np.pi
    phi = np.random.uniform(0, 1, size = (n,)) * 2. * np.pi
    
    x1 = r * np.sin(phi)
    x2 = r * np.cos(phi)
    x3 = phi
    
    return np.array([x1, x2, x3])

def generate_affine_6_36_space(n = 200):
    indep_var = generate_d_indep_var(6, n)
    
    param = [None]*36
    for i in range(5):
        param[2*i] = indep_var[i + 1] * np.cos(2. * np.pi * indep_var[i])
        param[2*i + 1] = indep_var[i + 1] * np.sin(2. * np.pi * indep_var[i])
    param[2*5] = indep_var[0] * np.cos(2. * np.pi * indep_var[5])
    param[2*5 + 1] = indep_var[0] * np.sin(2. * np.pi * indep_var[5]) 

    for j in range(12,36):
        param[j] = param[j-12]
    return np.array(param)

def generate_swiss_roll(n = 500):
    r = 2. * np.pi * np.random.uniform(0, 1, size=(n,)) - np.pi
    phi = np.random.uniform(0, 1, size=(n,)) * 2. * np.pi
    
    x1 = phi * np.sin(2.5 * phi)
    x2 = r
    x3 = phi * np.cos(2.5 * phi)
    
    return np.array([x1, x3, x3])

def generate_same_dim_space(k, n = 200):
    indep_var = generate_d_indep_var(k, n, func = np.vectorize(lambda x: 5.* x - 2.5))
    indep_var = np.array(indep_var)
    return indep_var

def generate_k_hypercube(k, n=200):
    indep_var = generate_d_indep_var(k, n)
    indep_var[k-1] = [0] * n
    return np.array(indep_var)

def generate_multivariate_gaussian(k, n = 200):
    return np.array([np.random.normal(0, 1, size = (n,)) for i in range(k)])

def generate_kd_curve(k, n = 200):
    indep_var = generate_d_indep_var(1, n, func=np.vectorize(lambda x: x* 2. * np.pi))
    
    param = [None]*k
    for i in range(k):
        param[i] = indep_var[0] / (2. * np.pi)
        for k in range(0, i):
            param[i] += np.sin((k + 1) * indep_var[0])
        param[i] /= (1. * (i + 1.))
        
    return np.array(param)